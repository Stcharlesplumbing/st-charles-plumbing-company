St Charles Plumbing Company is own and operated by a 3rd generation licensed and bonded master plumber. We stand behind our work and guarantee no leaks when a job is complete. We serves the greater St Louis metro area in Missouri doing everything from bathroom remodels to sink reconnects.

Address: 3441 Ipswich Ln, Saint Charles, MO 63301, USA

Phone: 636-336-6944

Website: http://stcharlesplumbingco.com
